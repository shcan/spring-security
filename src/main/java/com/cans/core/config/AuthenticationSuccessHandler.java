package com.cans.core.config;

import com.alibaba.fastjson.JSON;
import com.cans.core.dto.ResDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Auther: shencan
 * Date: 2017/10/10
 * Description: 配置登陆返回JSON
 */
public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
            throws IOException, ServletException {

        ResDto<String> success = ResDto.success("登陆成功!");

        String s = JSON.toJSONString(success);

        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().print(s);
        response.getWriter().flush();
    }

}
