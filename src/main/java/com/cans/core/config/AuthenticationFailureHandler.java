package com.cans.core.config;

import com.alibaba.fastjson.JSON;
import com.cans.core.dto.ResDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Auther: shencan
 * Date: 2017/10/10
 * Description:
 */
public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        ResDto<String> fait = ResDto.fait(exception.getMessage());

        String s = JSON.toJSONString(fait);

        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().print(s);
        response.getWriter().flush();

    }
}
