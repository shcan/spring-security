package com.cans.core.service.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;

/**
 * Service基类
 * @param <T>
 * @param <ID>
 */
public interface IBaseService<T,ID extends Serializable> {
    /**
     * 保存或更新
     *
     * @param t
     */
    public void saveOrUpdate(T t);

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    public Page<T> findPage(Pageable page);

    /**
     * 通过ID获取
     *
     * @param id
     * @return
     */
    public T findOne(String id);

    /**
     * 通过ID删除
     *
     * @param id
     */
    public void delete(String id);

    /**
     * 通过bean删除
     *
     * @param user
     */
    public void delete(T user);

    /**
     *
     * @param t
     * @return
     */
    public List<T> findAll(T t);

    /**
     *
     * @return
     */
    public List<T> findAll();

    /**
     *
     * @param t
     * @return
     */
    public T findOne(T t);
}
