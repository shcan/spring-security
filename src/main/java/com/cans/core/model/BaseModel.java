package com.cans.core.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Auther: shencan
 * Date: 2017/10/3
 * Description: model基础类其它model层必须依赖此类
 */
@MappedSuperclass
public class BaseModel implements Serializable{

    // ID使用UUID生成
    @Id
    @GeneratedValue(generator = "systemUUID")
    @GenericGenerator(name = "systemUUID",strategy = "uuid")
    private String id;

    // 创建时间
    @Temporal(TemporalType.TIME)
    @Column(name = "create_time")
    private Date createTime;

    // 最后修改时间
    @Temporal(TemporalType.TIME)
    @Column(name = "last_change_time")
    private Date lastChangeTime;


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastChangeTime() {
        return lastChangeTime;
    }

    public void setLastChangeTime(Date lastChangeTime) {
        this.lastChangeTime = lastChangeTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
