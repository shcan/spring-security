package com.cans.core.util;

import org.springframework.data.domain.PageRequest;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by 31733 on 2017/5/20.
 */
public class PageUtils {

    public static PageRequest buildPage(HttpServletRequest request){

        String pageStr = request.getParameter("page");
        String sizeStr = request.getParameter("size");
        request.getParameter("order");
        request.getParameter("sort");

        int page = pageStr == null ? 0 : Integer.valueOf(pageStr);
        int size = pageStr == null ? 10 : Integer.valueOf(sizeStr);

        PageRequest pageRequest = new PageRequest(page, size);
        return pageRequest;
    }
}
