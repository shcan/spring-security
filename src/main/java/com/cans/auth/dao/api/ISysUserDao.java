package com.cans.auth.dao.api;

import com.cans.auth.model.SysUser;
import com.cans.core.dao.api.IBaseDao;
import org.springframework.stereotype.Repository;

/**
 * Auther: shencan
 * Date: 2017/10/3
 * Description:
 */
@Repository("sysUserDao")
public interface ISysUserDao extends IBaseDao<SysUser,String> {
}
