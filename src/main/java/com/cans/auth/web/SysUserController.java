package com.cans.auth.web;

import com.cans.auth.model.SysUser;
import com.cans.auth.service.api.ISysUserService;
import com.cans.core.dto.ResDto;
import com.cans.core.util.PageUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Auther: shencan
 * Date: 2017/10/3
 * Description:
 */
@RestController
public class SysUserController {

    @Resource
    private ISysUserService sysUserService;


    @RequestMapping("pageList")
    public ResDto userList(HttpServletRequest request){
        PageRequest page = PageUtils.buildPage(request);
        Page<SysUser> users = sysUserService.findPage(page);
        return ResDto.success(users);
    }

}
