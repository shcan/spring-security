package com.cans.auth.model;

import com.cans.core.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Auther: shencan
 * Date: 2017/10/3
 * Description: 角色类
 */
@Entity
@Table(name = "sys_role")
public class SysRole extends BaseModel {

    // 角色名称
    @Column(name = "role_name")
    private String  roleName;

    // 角色编码
    @Column(name = "role_code")
    private String roleCode;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}
