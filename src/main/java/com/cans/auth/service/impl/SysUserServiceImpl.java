package com.cans.auth.service.impl;

import com.cans.auth.dao.api.ISysUserDao;
import com.cans.auth.model.SysUser;
import com.cans.auth.service.api.ISysUserService;
import com.cans.core.dao.api.IBaseDao;
import com.cans.core.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;

/**
 * Auther: shencan
 * Date: 2017/10/3
 * Description:
 */
@Service("sysUserService")
@Transactional
public class SysUserServiceImpl extends BaseServiceImpl<SysUser,String> implements ISysUserService {

    @Resource
    private ISysUserDao sysUserDao;

    @Override
    public IBaseDao<SysUser, String> getDaoImpl() {
        return sysUserDao;
    }

}
