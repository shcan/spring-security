package com.cans.auth.service.impl;

import com.cans.auth.dao.api.ISysUserDao;
import com.cans.auth.model.SysUser;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;

/**
 * Auther: shencan
 * Date: 2017/10/4
 * Description:
 */
public class CustomUserService implements UserDetailsService {

    @Resource
    private ISysUserDao sysUserDao;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        SysUser user = new SysUser();
        user.setUserName(s);
        //创建匹配器，即如何使用查询条件
        ExampleMatcher matcher = ExampleMatcher.matching() //构建对象
                .withMatcher("userName", ExampleMatcher.GenericPropertyMatchers.storeDefaultMatching()); //姓名采用“开始匹配”的方式查询

        //创建实例
        Example<SysUser> ex = Example.of(user, matcher);

        return sysUserDao.findOne(ex);
    }
}
