package com.cans.auth.service.api;

import com.cans.auth.model.SysUser;
import com.cans.core.service.api.IBaseService;

/**
 * Auther: shencan
 * Date: 2017/10/3
 * Description:
 */
public interface ISysUserService extends IBaseService<SysUser,String> {
}
