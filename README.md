# spring-security
使用spring boot 整合spring security的前后端分离项目


## 框架技术
* spring boot
* spring security
* spring data jpa


本项目设计初衷是为前后端分离做准备的,后期准备用vuejs写一套简单的前端页面.
前端访问返回全部为JSON字符串,对spring security 的认证进行了修改